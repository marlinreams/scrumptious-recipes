from django.db import models


# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(max_length=200, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name + " by " + self.author


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    Abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name


class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    amount = models.FloatField(null=True)
    recipe = models.ForeignKey("Recipe", related_name="ingredients", on_delete=models.CASCADE, null=True)

    measure = models.ForeignKey("Measure", on_delete=models.PROTECT, null=True)

    food = models.ForeignKey("FoodItem", on_delete=models.PROTECT, null=True)

    def __str__(self):
        return str(self.amount) + " " + str(self.measure)


class Step(models.Model):
    recipe = models.ForeignKey("Recipe", related_name="steps", on_delete=models.CASCADE, null=True)
    order = models.SmallIntegerField()
    directions = models.CharField(max_length=300)

    def __str__(self):
        return str(self.order) + " " + str(self.directions)
